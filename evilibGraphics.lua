evilib.graphics = {}

ww = love.window.getWidth()
wh = love.window.getHeight()

function evilib.graphics.resetColor( ... )
	gr.setColor(255, 255, 255, 255)
end

function evilib.graphics.updateAnimations()
	for i, v in ipairs(evilib.graphics.drawable) do
		if v.class.name == "Image" then
			local currentAnimation = v.animations[v.currentAnimation]
			if v.playAnimation then
				local fs = 1
				if v.playBack then
					fs = -1
				end
				v.currentFrame = v.currentFrame + currentAnimation.frameSpeed * dt * fs * v.animationSpeed
			end
			if currentAnimation.frameMode == "once" then
				if v.currentFrame > currentAnimation.lastFrame + 1 then
					v.currentFrame = currentAnimation.lastFrame
					v.playAnimation = false
					currentAnimation.postFunction()
				end
			elseif currentAnimation.frameMode == "loop" then
				if v.currentFrame > currentAnimation.lastFrame + 1 then
					v.currentFrame = 1
				end
			elseif currentAnimation.frameMode == "bounce" then
				if not v.playBack then
					if v.currentFrame > currentAnimation.lastFrame + 1 then
						v.currentFrame = currentAnimation.lastFrame
						v.playBack = true
					end
				else
					if v.currentFrame < 1 then
						v.currentFrame = 1.99
						v.playBack = false
					end
				end
			end
		end
	end
end

evilib.graphics.drawable = {}
evilib.graphics.drawableToRemove = {}
evilib.graphics.cameras = {}
evilib.graphics.viewPorts = {}

function _orderDepth(a, b)
	return a.depth > b.depth
end

Graphics = class("Graphics")

function Graphics:initialize(x, y, width, height, angle, xoffset, yoffset, blend)
	self.x = x or 0
	self.y = y or 0 
	self.width = width or 1
	self.height = height or 1
	self.angle = angle or 0
	self.xoffset = xoffset or 0
	self.yoffset = yoffset or 0
	self.blend = blend or {255, 255, 255, 255}
	self.depth = 0
	table.insert(evilib.graphics.drawable, self)
	return self
end

function Graphics:setPosition(x, y)
	self.x = x or self.x
	self.y = y or self.y
	return self
end

function Graphics:move(x, y)
	self.x = self.x + x
	self.y = self.y + y
	return self
end

function Graphics:setSize(x, y)
	self.width = x or self.width
	self.height = y or self.height
	return self
end

function Graphics:addSize(x, y)
	self.width = self.width + x
	self.height = self.height + y
	return self
end

function Graphics:setAngle(angle)
	self.angle = angle
	return self
end

function Graphics:rotate(angle)
	self.angle = self.angle + angle
	return self
end

function Graphics:setBlend(r, g, b, a)
	self.blend = {r, g, b, a}
	return self
end

function Graphics:remove()
	table.insert(evilib.graphics.drawableToRemove, self)
end

Text = class("Text", Graphics)

function Text:initialize(t, font, x, y, width, height, angle, xoffset, yoffset, centered)
	self.text = t
	self.font = font
	self.textW = self.font:getWidth(self.text)
	self.textH = self.font:getHeight(self.text)
	self.centered = centered or false
	Graphics.initialize(self, x, y, width, height, angle, xoffset, yoffset)
end

function Text:setText(t)
	self.text = t
	self.textW = self.font:getWidth(self.text)
	self.textH = self.font:getHeight(self.text)
	return self
end

function Text:setFont(font)
	self.font = font
	self.textW = self.font:getWidth(self.text)
	self.textH = self.font:getHeight(self.text)
	return self
end

function Text:draw()
	gr.setFont(self.font)
	gr.setColor(unpack(self.blend))
	local xo, yo = 0, 0
	if self.centered then
		xo, yo = self.textW / 2, self.textH / 2
	end
	gr.print(self.text, self.x, self.y, self.angle, self.width, self.height, self.xoffset + xo, self.yoffset + yo)
	gr.setColor(255, 255, 255, 255)
end

Shape = class("Shape", Graphics)

function Shape.DRAW_CIRCLE(object)
	local obj = object--_DRAWING_OBJECT
	gr.setLineWidth(obj.lineWidth)
	gr.translate(obj.x, obj.y)
	gr.rotate(obj.angle)
	gr.circle(obj.mode, -obj.xoffset, -obj.yoffset, obj.radius, obj.segments)
	gr.rotate(-obj.angle)
	gr.translate(-obj.x, -obj.y)
	gr.setLineWidth(1)
end

function Shape.DRAW_LINE(object)
	local obj = object
	gr.setLineWidth(obj.lineWidth)
	gr.line(unpack[obj.points])
	gr.setLineWidth(1)
end

function Shape.DRAW_TRIANGLE(object)
	local obj = object--_DRAWING_OBJECT
end

function Shape:initialize(mode, x, y)
	Graphics.initialize(self, x, y)
	--self.t = t
	self.mode = mode
	self.lineWidth = 1
end

function Shape:getPoint(n)
	if self.t == "triangle" then
		return self.points[n]
	end
end

Rectangle = class("Rectangle", Shape)

function Rectangle:initialize(mode, x, y, width, height, angle, xoffset, yoffset)
	Shape.initialize(self, mode, x, y)
	self.width = width or 1
	self.height = height or 1
	self.angle = angle or 0
	self.xoffset = xoffset or 0
	self.yoffset = yoffset or 0
end

function Rectangle:draw()
	gr.setLineWidth(self.lineWidth)
	gr.translate(self.x, self.y)
	gr.rotate(self.angle)
	gr.rectangle(self.mode, -self.xoffset, -self.yoffset, self.width, self.height)
	gr.rotate(-self.angle)
	gr.translate(-self.x, -self.y)
	gr.setLineWidth(1)
end

Circle = class("Circle", Shape)

function Circle:initialize(mode, x, y, radius, segments, angle, xoffset, yoffset)
	Shape.initialize(self, mode, x, y)
	self.radius = radius or 16
	self.segments = segments or 16
	self.angle = angle or 0
	self.xoffset = xoffset or 0
	self.yoffset = yoffset or 0
end

function Circle:draw()
	gr.setLineWidth(self.lineWidth)
	gr.translate(self.x, self.y)
	gr.rotate(self.angle)
	gr.circle(self.mode, -self.xoffset, -self.yoffset, self.radius, self.segments)
	gr.rotate(-self.angle)
	gr.translate(-self.x, -self.y)
	gr.setLineWidth(1)
end

Line = class("Line", Shape)

function Line:initialize(width, x, y, angle, ...)
	Shape.initialize(self, x, y)
	self.lineWidth = width
	self.points = {}
	local a = {...}
	for i = 1, #a, 2 do
		table.insert(self.points, {x = a[i], y = a[i+1]})
	end
end

Polygon = class("Polygon", Shape)

function Polygon:initialize(mode, x, y, angle, ...)
	Shape.initialize(self, mode, x, y)
	self.points = {}
	local a = {...}
	for i = 1, #a, 2 do
		table.insert(self.points, a[i])
		table.insert(self.points, a[i+1])
	end
	self.angle = angle or 0
	self.xoffset = 0
	self.yoffset = 0
end

function Polygon:getPointsCount()
	return #self.points
end

function Polygon:getPoint(n)
	return self.points[n], self.points[n]
end

function Polygon:draw()
	--local newPoints = {}
	--[[for i = 1, #self.points - 1, 2 do
		table.insert(newPoints, self.points[i] + self.x)
		table.insert(newPoints, self.points[i + 1] + self.y)
	end]]
	gr.setLineWidth(self.lineWidth)
	gr.translate(self.x, self.y)
	gr.rotate(self.angle)
	if self.mode == "Line" then
		gr.line(self.points)
	else
		gr.polygon(self.mode, self.points)
	end
	gr.rotate(-self.angle)
	gr.translate(-self.x, -self.y)
	gr.setLineWidth(1)
end

Image = class("Image", Graphics)

function Image:initialize(img, x, y, width, height, angle, xoffset, yoffset)
	self.img = img
	self.width = img:getWidth()
	self.height = img:getHeight()
	self.animations = {}
	self:newAnimation("default", 0, 1, 0, self.width, self.height, "linear")
	self.currentAnimation = "default"
	self.currentFrame = 1
	self.playAnimation = false
	self.playBack = false
	self.animationSpeed = 1
	Graphics.initialize(self, x, y, width, height, angle, xoffset, yoffset)
end

function Image:newAnimation(name, framesY, frames, frameSpeed, frameWidth, frameHeight, type, postFunction)
	self.animations[name] = {}
	self.animations[name].frames = {}
	for i = 1, frames do
		self.animations[name].frames[i] = gr.newQuad((i - 1) * frameWidth, 0, frameWidth, frameHeight, self.img:getWidth(), self.img:getHeight())
	end
	self.animations[name].frameSpeed = frameSpeed
	self.animations[name].frameMode = type
	self.animations[name].lastFrame = frames
	self.animations[name].postFunction = postFunction or function() end
end

function Image:change(img)
	self.img = img
	return self
end

function Image:draw()
	gr.setColor(unpack(self.blend))
	gr.draw(self.img, self.animations[self.currentAnimation].frames[math.floor(self.currentFrame)], self.x, self.y, self.angle, self.width, self.height, self.xoffset, self.yoffset)
	gr.setColor(255, 255, 255, 255)
end


Camera = class("Camera")

function Camera:initialize(x, y, width, height, left, top, right, bottom, vpX, vpY)
	self.x = x
	self.y = y
	self.width = width
	self.height = height
	self.top = top
	self.bottom = bottom
	self.left = left
	self.right = right
	self.canvas = gr.newCanvas(width, height)
	self.viewPort = evilib.graphics.addViewPort(viewPort, vpX, vpY, self.width, self.height, self.canvas)
	table.insert(evilib.graphics.cameras, self)
end

function Camera:move(x, y, relative)
	local tx, ty = x, y
	if relative then
		tx = self.x + x
		ty = self.y + y
	end
	self.x = evilib.math.clamp(tx, left, right - self.width)
	self.y = evilib.math.clamp(ty, top, bottom - self.height)
end

function evilib.graphics.addViewPort(i, x, y, w, h, canvas)
	local v = {}
	v.x = x
	v.y = y
	v.canvas = canvas
	table.insert(evilib.graphics.viewPorts, v)
	return v
end