evilib.math = {}

sin = math.sin
cos = math.cos

function evilib.math.clamp(val, min, max)
	return math.min(math.max(val, min), max)
end

function evilib.math.clampVector(x, y, max, min)
	local d = math.sqrt(x*x + y*y)
	if d > len then
		local n = 1/d * len
		x, y = x * n, y * n
	end
	return x, y
end

function evilib.math.sign(n)
	if n == 0 then return 0 end
	return n / math.abs(n)
end

function evilib.math.choose( ... )
	arg = { ... }
	return arg[math.random(#arg)]
end									

function evilib.math.round(n)
	return math.floor(n + 0.5)
end

function evilib.math.div(n, d)
	return math.floor(n / d)
end

function evilib.math.pointDistance(ax, ay, bx, by)
	return math.sqrt(((bx - ax) ^ 2) + ((by - ay) ^ 2))
end

function evilib.math.pointDirection(ax, ay, bx, by)
	return math.atan2(by - ay, bx - ax)
end

function evilib.math.lx(len, dir)
	return math.cos(dir) * len
end

function evilib.math.ly(len, dir)
	return math.sin(dir) * len
end

div = evilib.math.div