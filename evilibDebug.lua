_FONT = gr.newFont("cour.ttf", 12)
_FONTH = _FONT:getHeight("#")
_FONTW = _FONT:getWidth("#")
_FONT:setFilter("nearest", "nearest")

evilib.debug = {}
evilib.log = {}
evilib.console = {}

evilib.activeDebug = nil

evilib.debug.info = ""
evilib.debug.draw = false

function evilib.debug.setFont(s)
	_FONT = gr.newFont("cour.ttf", s)
	_FONTH = _FONT:getHeight("#")
	_FONTW = _FONT:getWidth("#")
end

function evilib.debug.addMain()				
	evilib.debug.add("<MAIN DATA>")						
	evilib.debug.add("FPS: " .. tm.getFPS())
	evilib.debug.add("DT: " .. tm.getDelta())
	evilib.debug.add("<ADDITIONAL DATA>")
end

function evilib.debug.add(s)
	evilib.debug.info = evilib.debug.info .. s .. "\n"
end

function evilib.debug.draw()
	gr.print(evilib.debug.info, 0, 0)
end


evilib.log.lines = {}
evilib.log.draw = false

function evilib.log.add(s)
	table.insert(evilib.log.lines, s)
end

print = evilib.log.add

function evilib.log.draw()
	--[[for i = 1, #evilib.log.lines do
		if evilib.log.lines[i] ~= nil then
			gr.print(">" .. evilib.log.lines[i], 0, i * _FONTH - _FONTH)
		end
	end]]
	local y = 0
	for i = #evilib.log.lines - 20, #evilib.log.lines do
		if evilib.log.lines[i] ~= nil then
			gr.print(">" .. evilib.log.lines[i], 0, y)
			y = y + _FONTH
		end
	end
end

function evilib.drawDebug()
	gr.setColor(255, 255, 255)
	gr.setFont(_FONT)
	if evilib.activeDebug ~= nil then
		evilib.activeDebug.draw()
	end
end



evilib.console.line = ""
evilib.console.log = {}
evilib.console.logLineN = #evilib.console.log
evilib.console.lineCur = 1

function evilib.console.execute()
	local code, err = loadstring(evilib.console.line)
	if evilib.console.line ~= "exit" then
		table.insert(evilib.console.log, evilib.console.line)
		if code ~= nil then
			evilib.log.add(evilib.console.line)
			code()
			evilib.log.add("TRUE")
		else
			evilib.log.add(err)
		end
	else
		evilib.quit()
	end
	--evilib.input.clearString()
	evilib.console.line = ""
	evilib.console.lineCur = 1
end

function evilib.console.textInput(k)
	if evilib.activeDebug ~= evilib.console then return end
	local ec = evilib.console
	local ss = string.sub(ec.line, 1, ec.lineCur - 1)
	local es = string.sub(ec.line, ec.lineCur, #ec.line)
	ec.line = ss .. k .. es
	ec.lineCur = ec.lineCur + 1
end

function evilib.console.draw()
	evilib.log.draw()
	gr.print("CONSOLE>> " .. evilib.console.line, 0, wh - _FONTH)
	gr.print("|", _FONT:getWidth("CONSOLE>> ") + (evilib.console.lineCur - 1 - 0.5) * _FONTW, wh - _FONTH)
	--[[for i = 1, #evilib.console.line do
		gr.print(string.sub(evilib.console.line, i, i), i * 5, wh - _FONTH)
	end]]
end


function evilib.debugUpdate()
	local ed = evilib.debug
	local el = evilib.log
	local ec = evilib.console
	ed.info = ""
	ed.addMain()
	if keyDown("lctrl") then
		if keyPressed("f1") then
			evilib.activeDebug = nil
		end
		if keyPressed("f2") then
			evilib.activeDebug = ed
		end
		if keyPressed("f3") then
			evilib.activeDebug = el
		end
		if keyPressed("f4") then
			evilib.activeDebug = ec
			ec.logLineN = #ec.log + 1
			ec.lineCur = 1
			evilib.input.clearString()
		end
	end

	if evilib.activeDebug == ec then
		dt = 0
		if keyPressed("return") then
			ec.execute()
			ec.logLineN = #ec.log + 1
		end
		if keyPressed("backspace") then
			if ec.lineCur > 1 then
				local ss = string.sub(ec.line, 1, ec.lineCur - 2)
				local es = string.sub(ec.line, ec.lineCur, #ec.line)
				ec.line = ss .. es
				ec.lineCur = math.max(ec.lineCur - 1, 1)
			end
		end
		if keyPressed("up") then
			ec.logLineN = ec.logLineN - 1
			ec.logLineN = math.max(1, ec.logLineN)
			if ec.log[ec.logLineN] then
				ec.line = ec.log[ec.logLineN]
			else
				ec.line = ""
			end
			ec.lineCur = #ec.line + 1
		elseif keyPressed("down") then
			ec.logLineN = ec.logLineN + 1
			ec.logLineN = math.min(#ec.log + 1, ec.logLineN)
			if ec.log[ec.logLineN] then
				ec.line = ec.log[ec.logLineN]
			else
				ec.line = ""
			end
			ec.lineCur = #ec.line + 1
		elseif keyPressed("left") then
			ec.lineCur = math.max(ec.lineCur - 1, 1)
		elseif keyPressed("right") then
			ec.lineCur = math.min(ec.lineCur + 1, #ec.line + 1)
		end
		evilib.input.update()
	end
end