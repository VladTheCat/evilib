require "class"

evilib = {}

math.randomseed(os.time())

kb = love.keyboard
ms = love.mouse
gr = love.graphics
im = love.image
tm = love.timer
pi = math.pi
s2 = math.sqrt(2)

require "evilibDebug"
require "evilibGraphics"
require "evilibMath"
require "evilibInput"
require "evilibTile"

evilib.isAndroid = (love.system.getOS() == "Android")

function evilib.preUpdate(delta)
	evilib.graphics.drawableToRemove = {}
	wmx, wmy = ms.getPosition()
	mx, my = wmx, wmy
	for i, v in ipairs(evilib.graphics.cameras) do
		if wmx >= v.viewPort.x and wmy >= v.viewPort.y and
		   wmx < v.viewPort.x + v.width and wmy < v.viewPort.y + v.height then
			mx = wmx - v.viewPort.x + v.x
			my = wmy - v.viewPort.y + v.y
			break
		end
	end
	if keyPressed("backspace") then
		evilib.input.keystring = string.sub(evilib.input.keystring, 1, #evilib.input.keystring - 1)
	end
	evilib.debugUpdate()
	for i = 1, love.touch.getTouchCount() do
		local id, tx, ty, pressure = love.touch.getTouch(i)
		local x = tx * ww
		local y = ty * wh
		evilib.debug.add("Tocuh #"..id.." is down at ("..x..";"..y..")")
		for k, v in pairs(evilib.input.virtualKeys) do
			if x > v.x and y > v.y and x < v.x + v.w and y < v.y + v.h then
				v.isDown = true
				evilib.debug.add("key ".. v.name .. " isDown")
			end
		end
	end
end

function evilib.postUpdate(delta)
	for i, v in ipairs(evilib.graphics.drawableToRemove) do
		for j, w in ipairs(evilib.graphics.drawable) do
			if w == v then
				table.remove(evilib.graphics.drawable, j)
				break
			end
		end
	end
	if keyPressed('r') and keyDown('lctrl') then
		evilib.clear()
		love.load()
	end
	if keyPressed('escape') then
		evilib.quit()
	end
	evilib.input.update()
end

function evilib.draw()
	table.sort(evilib.graphics.drawable, _orderDepth)
	evilib.graphics.updateAnimations()
	for i, v in ipairs(evilib.graphics.cameras) do
		_activeCamera = v
		v.canvas:clear()
		gr.setCanvas(v.canvas)
		gr.translate(-v.x, -v.y)
		if gamePreDraw then
			gamePreDraw()
		end
		_DRAWING_OBJECT = vv
		for ii, vv in ipairs(evilib.graphics.drawable) do
			gr.setColor(unpack(vv.blend))
			vv:draw()
		end
		if gamePostDraw then
			gamePostDraw()
		end
		gr.translate(v.x, v.y)
		gr.setCanvas()
		gr.setColor(0, 0, 0, 255)
		gr.rectangle('line', v.viewPort.x, v.viewPort.y, v.width, v.height)
		gr.setColor(255, 255, 255, 255)
	end
	for i, v in ipairs(evilib.graphics.viewPorts) do
		gr.draw(v.canvas, v.x, v.y)
	end
	if gameDrawGUI then
		gameDrawGUI()
	end
	if evilib.isAndroid then
		for i, v in pairs(evilib.input.virtualKeys) do
			if v.visible then
				gr.setColor(255, 255, 255, 100)
				if v.image then
					gr.draw(v.image, v.x + v.w/2, v.y + v.h/2, 0, 1, 1, v.image:getWidth()/2, v.image:getHeight()/2)
				else
					gr.rectangle("fill", v.x, v.y, v.w, v.h)
					gr.setColor(0, 0, 0, 100)
					gr.print(v.name, v.x, v.y + 10)
				end
			end
			gr.setColor(255, 255, 255, 255)
		end
	end
	evilib.drawDebug()
end

function evilib.quit()
	love.event.push('quit')
end

function evilib.clear()
	evilib.graphics.drawable = {}
	evilib.graphics.viewPorts = {}
	evilib.graphics.cameras = {}
end

evilib.exit = evilib.quit

function love.load(arg)
	if arg then
		for i, v in ipairs(arg) do
			if v == "debug" then
				evilib.activeDebug = evilib.debug
			elseif v == "log" then
				evilib.activeDebug = evilib.log
			end
		end	
	end
	if gameLoad then
		gameLoad()
	end
end

function love.update(delta)
	dt = delta
	evilib.preUpdate(delta)
	if gameUpdate and (evilib.activeDebug ~= evilib.console) then
		gameUpdate(dt)
	end
	evilib.postUpdate(delta)
	evilib.debug.add(string.format(
[[
Console log length: %d
Console log line now: %d
]], #evilib.console.log, evilib.console.logLineN))
end

function love.draw()
	evilib.draw()
end
