require "evilibCore"


function gameLoad()
	evilib.input.vkeyAdd("go", 0, 100, 100, 100, nil, true)
	evilib.input.vkeyAdd("left", 300, 300, 100, 100, nil, true)
	evilib.input.vkeyAdd("right", 400, 300, 100, 100, nil, true)

	strip = gr.newImage("Strip.png")
	strip:setFilter("linear", "nearest")
	--back = Rectangle:new("fill", 0, 0, 10000, 10000):setBlend(120, 140, 160, 255)
	gr.setBackgroundColor(120, 120, 120)
	for i = 0, 20 do
		local x, y = math.random(800), math.random(400)
		local t = Image:new(strip, x, y, 3 + (y - 200) / 200, 3 + (y - 200) / 200, 0, 7, 21):setBlend(math.random(100, 255), math.random(100, 255), math.random(100, 255))
		t.depth = -t.y
		t:newAnimation("run", 0, 3, math.random(3, 15), 14, 21, "once", function() t.x = math.random() * 800 end)
		t.currentAnimation = "run"
		t.playAnimation = true
	end
	plr = Image:new(strip, 100, 100, 3, 3, 0, 7, 21):setBlend(255, 255, 255, 255)
	plr:newAnimation("run", 0, 3, 10, 14, 21, "bounce")
	plr.currentAnimation = "run"
	plr.playAnimation = true
	plr.point = {x = plr.x, y = plr.y}

	Rec = Rectangle:new("fill", 300, 300, 200, 50, pi/3, 0, 25):setBlend(90, 30, 160, 255)
	Rec.depth = -Rec.y
	Crc = Circle:new("line", 300, 400, 25, 10, 0, 0, 0)
	Poly = Polygon:new("Line", 100, 100, 0, -30, -30, 30, -20, 30, -5, 40, 0, 30, 5, 30, 20, -30, 30)
	Poly.lineWidth = 3
	Poly.speed = 0

	Cam = Camera:new(0, 0, 400, 300, 0, 0, 800, 600, 50, 50)
	Cam2 = Camera:new(400, 50, 400, 300, 0, 0, 800, 600, 450, 100)
	Cam3 = Camera:new(-50, 300, 450, 300, 0, 0, 800, 600, 0, 350)
end

function gameUpdate(dt)
	-- Image Update
	if mouseDown("l") then
		plr.point.x = mx
		plr.point.y = my
	end
	local dx, dy = plr.point.x - plr.x, plr.point.y - plr.y
	local dist = math.sqrt(dx^2 + dy^2)
	plr.x = plr.x + dx / 10
	plr.y = plr.y + dy / 10
	plr.animationSpeed = dist / 50
	plr:setSize(3 + (plr.y - 200) / 200, 3 + (plr.y - 200) / 200)
	plr.depth = -plr.y

	--Rectalgle update
	Rec:rotate(pi/2 * dt)

	--Circle update
	Crc:setAngle(evilib.math.pointDirection(Crc.x, Crc.y, mx, my))

	--Polygon update
	if keyDown("w") or evilib.input.vkeyDown("go") then
		Poly.speed = 100
	elseif keyDown("s") then
		Poly.speed = Poly.speed - 100 * dt 
	else
		Poly.speed = Poly.speed / 1.5
	end
	if keyDown("a") or evilib.input.vkeyDown("left") then Poly:rotate(-pi / 2 * dt * Poly.speed / 250) end
	if keyDown("d") or evilib.input.vkeyDown("right") then Poly:rotate(pi / 2 * dt * Poly.speed / 250) end
	Poly.x = Poly.x + evilib.math.lx(Poly.speed * dt, Poly.angle)
	Poly.y = Poly.y + evilib.math.ly(Poly.speed * dt, Poly.angle)


	evilib.debug.add("Mouse coordinates in window: (" .. wmx .. ";" .. wmy .. ")")
	evilib.debug.add("Mouse coordinates in views: (" .. mx .. ";" .. my .. ")")
end

function gamePreDraw()
	gr.setColor(100, 100, 100, 200)
	gr.rectangle("fill", 200, 200, 200, 100)
	gr.setColor(150, 150, 150)
	gr.print("PreDraw Function", 230, 230)
end
function gamePostDraw()
	gr.setColor(100, 100, 100, 200)
	gr.rectangle("fill", 50, 50, 200, 100)
	gr.setColor(150, 150, 150)
	gr.print("PostDraw Function", 70, 70)
end
function gameDrawGUI()
	gr.setColor(100, 100, 100, 200)
	gr.rectangle("fill", 0, 0, 200, 100)
	gr.setColor(150, 150, 150)
	gr.print("DrawGUI Function", 70, 70)
end