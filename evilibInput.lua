evilib.input = {}

evilib.input.keysPressed = { }
evilib.input.keysReleased = { }

evilib.input.buttonsPressed = { }
evilib.input.buttonsReleased = { }

evilib.input.virtualKeys = { }

evilib.input.keystring = ""

function evilib.input.clearString()
	evilib.input.keystring = ""
end

function evilib.input.keyPressed(key)
	if (evilib.input.keysPressed[key]) then
		return true
	else 
		return false
	end
end

function evilib.input.keyReleased(key)
	if (evilib.input.keysReleased[key]) then
		return true
	else
		return false
	end
end

function love.keypressed(key, unicode)
	evilib.input.keysPressed[key] = true
end

function love.keyreleased(key)
	evilib.input.keysReleased[key] = true
end

function evilib.input.mousePressed(key)
	if (evilib.input.buttonsPressed[key]) then
		return true
	else
		return false
	end
end

function evilib.input.mouseReleased(key)
	if (evilib.input.buttonsReleased[key]) then
		return true
	else
		return false
	end
end

function evilib.input.vkeyAdd(ind, x, y, w, h, img, visible, depth)
	evilib.input.virtualKeys[ind] = {}
	local vk = evilib.input.virtualKeys[ind]
	vk.name = ind
	vk.x = x
	vk.y = y
	vk.w = w
	vk.h = h
	vk.pressed = false
	vk.isDown = false
	vk.visible = visible
	vk.image = img
	vk.depth = depth or 0
	vk.depth = -vk.depth
end

function evilib.input.vkeyDown(n)
	if evilib.input.virtualKeys[n].isDown then
		return true
	end
	return false
end

function evilib.input.vkeyPressed(n)
	if evilib.input.virtualKeys[n].pressed then
		return true
	end
	return false
end

function love.mousepressed(x, y, b)
	evilib.input.buttonsPressed[b] = true
end

function love.mousereleased(x, y, b)
	evilib.input.buttonsReleased[b] = true
end

function love.touchpressed(ind, tx, ty, pressure)
	local x = tx * ww
	local y = ty * wh
	for i, v in pairs(evilib.input.virtualKeys) do
		if x > v.x and y > v.y and x < v.x + v.w and y < v.y + v.h then
			v.pressed = true
			print("key ".. v.name .. " was pressed")
		end
	end
end

function love.touchmoved(id, tx, ty, pressure)
	local x = tx * ww
	local y = ty * wh
	evilib.debug.add("Tocuh #"..id.." was moved at ("..x..";"..y..")")
	for i, v in pairs(evilib.input.virtualKeys) do
		if x > v.x and y > v.y and x < v.x + v.w and y < v.y + v.h then
			v.isMoved = true
			evilib.debug.add("key ".. v.name .. " isDown")
		else
			v.isMoved = false
		end
	end
end

if not evilib.isAndroid then
	love.touch = {}
	function love.touch.getTouchCount()
		return 0
	end
end

function love.textinput(t)
	evilib.input.keystring = evilib.input.keystring .. t
	evilib.console.textInput(t)
end

function evilib.input.update()
	evilib.input.keysPressed = { }
	evilib.input.keysReleased = { }
	evilib.input.buttonsPressed = { }
	evilib.input.buttonsReleased = { }
	for i, v in pairs(evilib.input.virtualKeys) do
		v.pressed = false
		v.isDown = false
	end
end

keyDown = love.keyboard.isDown
mouseDown = love.mouse.isDown
keyPressed = evilib.input.keyPressed
keyReleased = evilib.input.keyReleased
mousePressed = evilib.input.mousePressed
mouseReleased = evilib.input.mouseReleased