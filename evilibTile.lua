evilib.tilemap = {}

Tilemap = class("Tilemap")

function Tilemap:initialize(tileMap, tileWidth, tileHeight, tilesX, tilesY, hspeed, vspeed, depth, dynamicDepth)
	self.tileMap = tileMap
	self.quads = {}
	self.tileIndex = {}
	self.tileWidth = tileWidth
	self.tileHeight = tileHeight
	self.tilesX = tilesX
	self.tilesY = tilesY
	self.hspeed = hspeed
	self.vspeed = vspeed
	self.depth = depth
	self.dynamicDepth = dynamicDepth
	self.map = {}
	for x = 0, tilesX - 1 do
		self.map[x] = {}
		for y = 0, tilesY - 1 do
			self.map[x][y] = 0
		end
	end
	table.insert(evilib.graphics.drawable, self)
end

function Tilemap:addTileIndex(id, name, x, y, w, h)
	local iw, ih
	iw = self.tileMap:getWidtH()
	ih = self.tileMap:getHeight()
	self.quads[id] = love.graphics.newQuad(x, y, w, h, iw, ih)
	self.tileIndex[id] = {}
	local ti = self.tileIndex[id]
		ti.name = name
		
end

function Tilemap:draw()
	local startX, startY = div(_activeCamera.x, self.tileWidth), div(_activeCamera.y, self.tileHeight)
	local tilesX, tilesY = div(_activeCamera.width, self.tileWidth) + 1, div(_activeCamera.height, self.tileHeight) + 1
	for x = startX, startX + tilesX do
		if self.map[x] ~= nil then
			for y = startY, startY + tilesY do
				if self.map[x][y] ~= nil then
					local img = self.quads[self.map[x][y]]
					gr.draw(self.tileMap, img, x * self.tileWidth, y * self.tileHeight)
				end
			end
		end
	end
end